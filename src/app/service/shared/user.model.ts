export class User {
    username: string;
    email: string; 
    password: string;
    age: number;
    race: string;
    food: string;
    friendRequestPending : [];
    friendRequestAccepted: [];
    friendRequestSend: [];
}