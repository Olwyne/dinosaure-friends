import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse,HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { User } from './user.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  selectedUser: User = {
    username: '',
    email: '',
    password: '',
    age: null,
    race: '',
    food: '',
    friendRequestPending : [],
    friendRequestAccepted: [],
    friendRequestSend:[]
  };
  apiBaseUrl:string = environment.apiBaseUrl;
  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  //HttpMethods

  postUser(user: User){
    return this.http.post(this.apiBaseUrl+'/register',user,this.noAuthHeader);
  }

  login(authCredentials) {
    return this.http.post(this.apiBaseUrl + '/authenticate', authCredentials,this.noAuthHeader);
  }

  getUserProfile() {
    return this.http.get(this.apiBaseUrl + '/userProfile');
  }

  getUser(user) {
    let url = `${this.apiBaseUrl}/getUser/${user.id}`;
    return this.http.get(url);
  }

  editUser(user: User): Observable<any>  {
    return this.http.put(this.apiBaseUrl+'/edit', user).pipe(
      catchError(this.errorMgmt)
    )
  }

  addFriend(user, friend): Observable<any>  {
    user.friendRequestSend.push(friend._id)
    friend.friendRequestPending.push(user.id)
    let data = {
        user : user,
        friend : friend
    } 
    return this.http.put(this.apiBaseUrl+'/addFriend', data).pipe(
      catchError(this.errorMgmt)
    )
  }

  acceptFriend(user, friend)  {
    user.friendRequestPending = user.friendRequestPending.filter(item => item !== friend._id)
    user.friendRequestAccepted.push(friend._id)
    friend.friendRequestSend = user.friendRequestSend.filter(item => item !== user.id)
    friend.friendRequestAccepted.push(user.id)
    let data = {
        user : user,
        friend : friend
    } 
    return this.http.put(this.apiBaseUrl+'/acceptFriend', data).pipe(
      catchError(this.errorMgmt)
    )
  }

  removeFriend(user, friend)  {
    user.friendRequestAccepted = user.friendRequestAccepted.filter(item => item !== friend._id)
    friend.friendRequestAccepted = user.friendRequestAccepted.filter(item => item !== user.id)
    let data = {
        user : user,
        friend : friend
    } 
    return this.http.put(this.apiBaseUrl+'/removeFriend', data).pipe(
      catchError(this.errorMgmt)
    )
  }
  
  cancelFriend(user, friend)  {
    user.friendRequestSend = user.friendRequestSend.filter(item => item !== friend._id)
    friend.friendRequestPending = user.friendRequestPending.filter(item => item !== user.id)
    
    let data = {
        user : user,
        friend : friend
    } 
    return this.http.put(this.apiBaseUrl+'/cancelFriend', data).pipe(
      catchError(this.errorMgmt)
    )
  }

  checkFriend(user, friend): Observable<any>  {
    let data = {
        user : user.id,
        friend : friend._id
    }
    let url = `${this.apiBaseUrl}/getUser/${data}`;
    return this.http.get(url).pipe(
      catchError(this.errorMgmt)
    )
  }

  deleteAccount(user : User) {
    let url = `${this.apiBaseUrl}/delete/${user.email}`;
    return this.http.delete(url);
  }

  //Helper Methods

  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  deleteToken() {
    localStorage.removeItem('token');
  }

  getUserPayload() {
    var token = this.getToken();
    if (token) {
      var userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    }
    else
      return null;
  }

  isLoggedIn() {
    var userPayload = this.getUserPayload();
    if (userPayload)
      return userPayload.exp > Date.now() / 1000;
    else
      return false;
  }

   // Error handling 
   errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
