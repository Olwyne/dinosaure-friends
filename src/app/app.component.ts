import { Component, OnInit } from '@angular/core';
import { UserService } from './service/shared/user.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Dino Friends';
  userDetails= {
    username:null,
    race:null,
    image:10
  };
  image=10;
  constructor(private userService: UserService, public router: Router) { 
    router.events.subscribe((val) => {
      if(router.url!="/login" && router.url!="/signup" && router.url!="/"){
        this.checkUser()
      }
  });

  }

  ngOnInit() {
    
  }

  //check user connect
  checkUser(){
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
        this.userDetails.image=this.checkPhoto()
      },
      err => { 
        console.log(err);
      }
    );
  }


  //check user photo
  checkPhoto(){
    if(this.userDetails.race == 'Parasaurolophus'){
      this.image= 1
      return 1
    }
    else if(this.userDetails.race == 'Stegosaure'){
      this.image= 2
      return 2
    }
    else if(this.userDetails.race == 'Euoplocephalus'){
      this.image= 3
      return 3
    }
    else if(this.userDetails.race == 'Spinosaure'){
      this.image= 4
      return 4
    }
    else if(this.userDetails.race == 'Hypsilophodon'){
      this.image= 5
      return 5
    }
    else if(this.userDetails.race == 'Pterosaure'){
      this.image= 6
      return 6
    }
    else if(this.userDetails.race == 'Brachiosaure'){
      this.image= 7
      return 7
    }
    else if(this.userDetails.race == 'Triceratops'){
      this.image= 8
      return 8
    }
    else if(this.userDetails.race == 'Tyrannosaure'){
      this.image= 9
      return 9
    }
    else if (this.userDetails.race == null){
      this.image= 10
      return 10
    }
  }

}
