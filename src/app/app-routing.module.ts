import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserListComponent } from './components/user-list/user-list.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';

import { UserComponent } from './components/user/user.component';
import { SignUpComponent } from './components/user/sign-up/sign-up.component';
import { SignInComponent } from './components/user/sign-in/sign-in.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AuthGuard } from './service/auth/auth.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'edit', component:  UserEditComponent,canActivate:[AuthGuard] },
  { path: 'list', component:  UserListComponent,canActivate:[AuthGuard] },
  { path: 'signup', component: UserComponent, children: [{ path: '', component: SignUpComponent }]},
  { path: 'login', component: UserComponent, children: [{ path: '', component: SignInComponent }]},
  { path: 'userprofile', component: UserProfileComponent,canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }