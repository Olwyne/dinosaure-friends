import { Component, OnInit, NgZone } from '@angular/core';
import { UserService } from '../../service/shared/user.service'

@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements OnInit {
	showSucessMessage: boolean;
	serverErrorMessages: string;
	userDetails:any = [];
	list:any = [];
	UserRace:any = ['Tyrannosaure', 'Brachiosaure', 'Pterosaure', 'Spinosaure', 'Triceratops', 'Stegosaure', 'Hypsilophodon', 'Euoplocephalus', 'Parasaurolophus']
	constructor(private userService: UserService) { }

	ngOnInit() {
		this.userService.getUserProfile().subscribe(
			res => {
				this.userDetails = res['user'];
				this.readUser()
			},
			err => { 
				console.log(err);
			}
		);
	}

	//get list of all user except the current user
	readUser(){
		this.userService.getUser(this.userDetails).subscribe(
			res => {
					this.list=res;
			},
			err => { 
				console.log(err);
			}
		);  
	}

	//click on add friend
	addFriend(friend)
	{
		this.userService.addFriend(this.userDetails,friend).subscribe(
			res => {
				console.log(res)
			},
			err => { 
				console.log(err);
			}
		);
	}

	//check for the state of the button friend
	checkFriend(friend){
		let button;
		friend.friendRequestPending.forEach(element => {if(element==this.userDetails.id){button=1}});
		friend.friendRequestSend.forEach(element => {if(element==this.userDetails.id){button=2}});
		friend.friendRequestAccepted.forEach(element => {if(element==this.userDetails.id){button=3}});
		if(button >=1){
			return button;
		}
		else{
			return 0;
		}
	}

	//click from delete friend
	removeFriend(friend){
		this.userService.removeFriend(this.userDetails,friend).subscribe(
			res => {
				console.log(res)
			},
			err => { 
				console.log(err);	
			}
		);
	}

	//click from accept friend
	acceptFriend(friend){
		this.userService.acceptFriend(this.userDetails,friend).subscribe(
			res => {
				console.log(res)

			},
			err => { 
				console.log(err);
			}
		);
	}

	//click from cancel request friend
	cancelFriend(friend){
		this.userService.cancelFriend(this.userDetails,friend).subscribe(
			res => {
				console.log(res)
			},
			err => { 
				console.log(err);
			}
		);
	}
}
