import { Component, OnInit, NgZone } from '@angular/core';
import { NgForm } from '@angular/forms';


import { UserService } from '../../service/shared/user.service'

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})

export class UserEditComponent implements OnInit {
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  showSucessMessage: boolean;
  serverErrorMessages: string;
  userDetails;
  UserRace:any = ['Tyrannosaure', 'Brachiosaure', 'Pterosaure', 'Spinosaure', 'Triceratops', 'Stegosaure', 'Hypsilophodon', 'Euoplocephalus', 'Parasaurolophus']
  UserFood:any = ['omnviore', 'vegetarian', 'vegan', 'carnivore']
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
        this.userService.selectedUser = {
          username: this.userDetails.username,
          email:  this.userDetails.email,
          password: this.userDetails.password,
          age:  this.userDetails.age,
          race:  this.userDetails.race,
          food:  this.userDetails.food,
          friendRequestPending : this.userDetails.friendRequestPending,
          friendRequestAccepted: this.userDetails.friendRequestAccepted,
          friendRequestSend: this.userDetails.friendRequestSend
        };
      },
      err => { 
        console.log(err);
        
      }
    );
  }

  //submit form
  onSubmit(form: NgForm) {
    let user = {
      username: form.value.username,
      email: form.value.email,
      password: form.value.password,
      age: form.value.age,
      race: form.value.race,
      food: form.value.food,
      friendRequestPending : this.userDetails.friendRequestPending,
      friendRequestAccepted: this.userDetails.friendRequestAccepted,
      friendRequestSend: this.userDetails.friendRequestSend
    }
    this.userService.editUser(user).subscribe(
      res => {
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 4000);
      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
        }
        else
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
      }
    );
  }



}
