import { Component, OnInit, NgZone } from '@angular/core';
import { NgForm } from '@angular/forms';


import { UserService } from '../../../service/shared/user.service'

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  showSucessMessage: boolean;
  serverErrorMessages: string;
  UserRace:any = ['Tyrannosaure', 'Brachiosaure', 'Pterosaure', 'Spinosaure', 'Triceratops', 'Stegosaure', 'Hypsilophodon', 'Euoplocephalus', 'Parasaurolophus']
  UserFood:any = ['omnviore', 'vegetarian', 'vegan', 'carnivore']
  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  //submit form
  onSubmit(form: NgForm) {
    let user = {
      username: form.value.username,
      email: form.value.email,
      password: form.value.password,
      age: form.value.age,
      race: form.value.race,
      food: form.value.food,
      friendRequestPending : null,
      friendRequestAccepted:null,
      friendRequestSend: null,
    }
    this.userService.postUser(user).subscribe(
      res => {
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 4000);
        this.resetForm(form);
      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
        }
        else
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
      }
    );
  }

  //reset form
  resetForm(form: NgForm) {
    this.userService.selectedUser = {
      username: '',
      email: '',
      password: '',
      age: null,
      race: '',
      food: '',
      friendRequestPending : null,
      friendRequestAccepted: null,
      friendRequestSend: null
    };
    form.resetForm();
    this.serverErrorMessages = '';
  }

}
