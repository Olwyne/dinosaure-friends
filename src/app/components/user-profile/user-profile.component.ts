import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/shared/user.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  userDetails;
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
      },
      err => { 
        console.log(err);
      }
    );
  }
  

  //delete account
 	deleteAccount(){
		if(window.confirm('Are you sure?')) {
			this.userService.deleteAccount(this.userDetails).subscribe(
        res => {
          this.onLogout();
        },
        err => { 
          console.log(err);
        }
			);
		}
  }
  

  //log out
  onLogout(){
    this.userDetails = undefined;
    this.userService.deleteToken();
    this.router.navigateByUrl('/login');
  }

}
