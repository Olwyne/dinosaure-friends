# dino-friends-app

- Run `npm install` to install required dependencies.
- Run `ng serve` to run the angular app
- Start the backend
  - `cd server` to enter into the backend folder
  - `npm install` to install required dependencies.
  - `nodemon serve` to start the nodemon server
  - `mongod` to start the mongoDB shell

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.